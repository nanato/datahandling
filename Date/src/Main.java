/*
* Дана строка вида "28 Февраль 2015" или “28 February 2015” (*"28 февраля 2015").
* Необходимо сконвертировать ее в вид “28/фев/15” или "28/Feb/15".
* Локаль учитывать не нужно (* нужно). Использовать SimpleDateFormat или DateTimeFormatter
*/

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
public class Main {
    private static DateFormatSymbols myDateFormatSymbols = new DateFormatSymbols() {

        public String[] getMonths() {
            return new String[]{"января", "февраля", "марта", "апреля", "мая", "июня",
                    "июля", "августа", "сентября", "октября", "ноября", "декабря"};
        }

    };

    public static void main(String[] args) throws ParseException {

        String strDate = "28 Февраль 2015";  //to -> 28/Feb/15

        Date currentDate = new Date();       //Получаем дату
        SimpleDateFormat dateFormat;  //Объект SimpleDateFormat, позволяет определять шаблоны форматирования

        //Текущая дата
        dateFormat = new SimpleDateFormat("dd MMMM yyyy");  //Создание паттерна
        System.out.println("Текущая дата:" + dateFormat.format(currentDate));

        //Шаблоны дат
        SimpleDateFormat fromUser = new SimpleDateFormat("dd MMM yyyy");
        SimpleDateFormat myFormat = new SimpleDateFormat("dd/MMM/yy",
                Locale.ENGLISH);
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd MMMM yyyy", myDateFormatSymbols);


        try {

            System.out.println(dateFormat2.format(fromUser.parse(strDate)));
            System.out.println(myFormat.format(fromUser.parse(strDate)));

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
