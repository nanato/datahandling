/*
Дана строка, содержащая "обычный" текст, т.е. слова, знаки препинания, переносы строк и т.п., например, "Ребе, Ви случайно не знаете, сколько тогда Иуда получил по нынешнему курсу?".
Необходимо получить строку, в которой содержатся только слова из исходной строки, разделенные знаком переноса строки, в нижнем регистре
Тут же находим самое короткое слово, самое длинное слово
 */
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
        private static String max(String par)
        {
            String[]words = par.split(" ");
            String maxstring="";
            for(String word: words)
            {
                if(word.length()>maxstring.length())
                    maxstring=word;
            }
            return(maxstring);
        }
        private static String min (String par)
        {
            String[]words = par.split(" ");
            String minstring=" ";
            for(String word: words)
            {
                if(!(word.length() >= minstring.length()))
                    minstring=word;
            }
            return(minstring);
        }

        static public void main(String args[])
        {
            String str="Ребе, Ви случайно не знаете, сколько тогда Иуда получил по нынешнему курсу?";
            Pattern p = Pattern.compile("[а-яА-Я]+");
            Matcher m = p.matcher(str);
            while (m.find()) System.out.println(str.substring(m.start(), m.end()).toLowerCase());
            System.out.println("Самое короткое слово - " + min(str));
            System.out.println("Самое длинное слово - " + max(str));
        }
    }
