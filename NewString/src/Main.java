/*
Дана строка-шаблон, содержащая заготовку письма. Например,
"Уважаемый, $userName, извещаем вас о том, что на вашем счете $номерСчета скопилась сумма, превышающая стоимость $числоМесяцев месяцев пользования нашими услугами. Деньги продолжают поступать. Вероятно, вы неправильно настроили автоплатеж. С уважением, $пользовательФИО $должностьПользователя"
Также дана одна пара строк. templateKey и templateValue. Необходимо в строке заменить все placeholders (строки $имяШаблона, т.е. '$' + templateKey) на значения из templateValue

 */
import java.util.HashMap;
import java.util.Map;
public class Main {
    public static void main(String[] args){

        String str = "Уважаемый, $userName, извещаем вас о том, что на вашем счете $номерСчета скопилась сумма," +
                " превышающая стоимость $числоМесяцев месяцев пользования нашими услугами. Деньги продолжают поступать." +
                " Вероятно, вы неправильно настроили автоплатеж. С уважением, $пользовательФИО , $должностьПользователя";

        doIT(str);
    }

    public static void doIT(String str) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("userName", "Alex");
        map.put("номерСчета", "10-1542097");
        map.put("числоМесяцев", "4");
        map.put("пользовательФИО", "Петров Г. О.");
        map.put("должностьПользователя", "Менеджер");
        for (Map.Entry<String, String> entry : map.entrySet()) {
            str = str.replace("$" + entry.getKey(), entry.getValue());
        }
        System.out.println(str);
    }
}
