/*
Дана строка, содержащая в себе, помимо прочего, номера телефонов.
 Необходимо удалить из этой строки префиксы локальных номеров, соответствующих Ижевску.
 Например, из "+7 (3412) 517-647" получить "517-647"; "8 (3412) 4997-12" > "4997-12"; "+7 3412 90-41-90" > "90-41-90"
 */
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class DataH {
    public static void main(String[] args) {

        String str = "+7 (3412) 517-647, 8 (3412) 4997-12, +7 3412 90-41-90";

        Pattern pattern = Pattern.compile("\\d{3}-\\d{3}|\\d{4}-\\d{2}|\\d{2}-\\d{2}-\\d{2}");
        Matcher ada = pattern.matcher(str);
        while (ada.find()) System.out.println(str.substring(ada.start(), ada.end()));
    }
}